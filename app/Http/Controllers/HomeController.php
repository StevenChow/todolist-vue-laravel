<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function spa()
    {
        $metaTitle = "Simple Todolist";
        return view('welcome', compact('metaTitle'));
    }
}
